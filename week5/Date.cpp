/*********************************************************************
** Author: Carol Toro
** Date: 2/4/2015
** Description: Assignment 5 - Project
** This program asks the user to enter a date and then prompts the user 
** to choose between 4 options: 3 options to print the date in different
** formats, and 1 option to quit the program. 
*********************************************************************/

#include <iostream>
#include "Date.hpp"
#include <iomanip>
#include <string>
using namespace std;

int main()
{
	//Define user variables
	int userMonth,
		userDay,
		userYear,
		userChoice;

	//Prompt user & get input
	cout << "Enter a month as number 1-12: ";
	cin >> userMonth;
	cout << "Enter a day as a number 1-31: ";
	cin >> userDay;
	cout << "Enter a year in four digit format: ";
	cin >> userYear;

	//Create Date object
	Date userDate(userMonth, userDay, userYear);

	//Print Menu to Screen

	do
	{
		//Display Menu & get user's choice
		cout << "\n Menu of options \n";
		cout << "1. Print date in format #1\n";
		cout << "2. Print date in format #2\n";
		cout << "3. Print date in format #3\n";
		cout << "4. Quit\n";
		cout << "Enter your choice (1-4): ";
		cin >> userChoice;

		if (userChoice != 4)
		{
			switch (userChoice)
			{
			case 1: userDate.print1();
				break;
			case 2: userDate.print2();
				break;
			case 3: userDate.print3();
				break;
			}
		}

	} while (userChoice != 4);

	return 0;
}

Date::Date() //Default constructor
{
	month = day = 1;
	year = 2001;
}

Date::Date(int dateMonth, int dateDay, int dateYear)
{
	setMonth(dateMonth);
	setDay(dateDay);
	setYear(dateYear);
}

void Date::setMonth(int dateMonth)
{
	if ((dateMonth >= 1) && (dateMonth <= 12))
	{
		month = dateMonth;
	}
	else
	{
		month = 0;
		cout << "That month value is invalid.\n";
	}
}
void Date::setDay(int dateDay)
{
	//Check February
	if (month == 2)
	{
		if ((dateDay >= 1) && (dateDay <= 29))
		{
			day = dateDay;
		}
		else
		{
			day = 0;
			cout << "That day value is invalid for that month.\n";
		}
	}
	//Check April, June, September, & November
	else if ((month == 4) || (month == 6) || (month == 9) || (month == 11))
	{
		if ((dateDay >= 1) && (dateDay <= 30))
		{
			day = dateDay;
		}
		else
		{
			day = 0;
			cout << "That day value is invalid for that month.\n";
		}
	}
	//Check January, March, May, July, August, October, December
	else if ((month == 1) || (month == 3) || (month == 5) || (month == 7) || (month == 8) || (month == 10) || (month == 12))
	{
		if ((dateDay >= 1) && (dateDay <= 31))
		{
			day = dateDay;
		}
		else
		{
			day = 0;
			cout << "That day value is invalid for that month.\n";
		}
	}

}

void Date::setYear(int dateYear)
{
	
	year = dateYear;
	
}

/*********************************************************************
** Description: This method transforms a 4 digit year into a 2 digit
** year and prints out the date in the first format: 01/01/01
** Parameters: n/a
*********************************************************************/
void Date::print1()
{
	int twoDigitYear,
		fourDigitYear;

	fourDigitYear = year;
	//Conditional to get 2 digit year
	twoDigitYear = fourDigitYear < 2000 ? fourDigitYear - 1900 : fourDigitYear - 2000;

	//Print format 1
	cout << month << "/" << day << "/" << setw(2) << setfill('0') << twoDigitYear << endl;
	
	/* Original implementation to get 2 digit year
		//Convert year to string
		string strYear = to_string(year);
		//Get last two digits of year
		strYear = strYear.substr(2, 3);
		//Print
		cout << month << "/" << day << "/" << strYear << endl;
	*/
}
/*********************************************************************
** Description: This method prints the date into the 2nd format:
** January 01, 2001
** Parameters: n/a
*********************************************************************/
void Date::print2()
{
	switch (month)
	{
		case 1: cout << "January " << day << ", " << year << endl;
			break;
		case 2: cout << "February " << day << ", " << year << endl;
			break;
		case 3: cout << "March " << day << ", " << year << endl;
			break;
		case 4: cout << "April " << day << ", " << year << endl;
			break;
		case 5: cout << "May " << day << ", " << year << endl;
			break;
		case 6: cout << "June " << day << ", " << year << endl;
			break;
		case 7: cout << "July " << day << ", " << year << endl;
			break;
		case 8: cout << "August " << day << ", " << year << endl;
			break;
		case 9: cout << "September " << day << ", " << year << endl;
			break;
		case 10: cout << "October " << day << ", " << year << endl;
			break;
		case 11: cout << "November " << day << ", " << year << endl;
			break;
		case 12: cout << "December " << day << ", " << year << endl;
			break;
	}
}
/*********************************************************************
** Description: This method prints the date into the third format:
** 01 January 2001
** Parameters: n/a
*********************************************************************/
void Date::print3()
{
	switch (month)
	{
	case 1: cout << day << " January " << year << endl;
		break;
	case 2: cout << day << " February " << year << endl;
		break;
	case 3: cout << day << " March " << year << endl;
		break;
	case 4: cout << day << " April " << year << endl;
		break;
	case 5: cout << day << " May " << year << endl;
		break;
	case 6: cout << day << " June " << year << endl;
		break;
	case 7: cout << day << " July " << year << endl;
		break;
	case 8: cout << day << " August " << year << endl;
		break;
	case 9: cout << day << " September " << year << endl;
		break;
	case 10: cout << day << " October " << year << endl;
		break;
	case 11: cout << day << " November " << year << endl;
		break;
	case 12: cout << day << " December " << year << endl;
		break;
	}
}