/*********************************************************************
** Author: Carol Toro
** Date: 2/3/2015
** Description: Assignment 5 - Exercise #2
** This program ask the users to enter the height, length, and width of a box
** and calculates the volume of the box as well as the surface area of the
** box.
*********************************************************************/

#include <iostream>
#include "Box.hpp"
using namespace std;

int main()
{
	double userHeight,
		userWidth,
		userLength,
		boxVolume,
		boxSurfaceArea;

	//Ask & get user input for box dimensions
	cout << "Enter the height of the box: ";
	cin >> userHeight;
	cout << "Enter the width of the box: ";
	cin >> userWidth;
	cout << "Enter the length of the box: ";
	cin >> userLength;

	//Create box object
	Box userBox(userHeight, userWidth, userLength);

	//Call the volume & surface area methods
	boxVolume = userBox.getVolume();
	boxSurfaceArea = userBox.getSurfaceArea();

	//Print volume & surface area
	cout << "The volume of the box is " << boxVolume << endl;
	cout << "The surface area of the box is " << boxSurfaceArea << endl;

	return 0;
}

Box::Box() //Default contructor to initialize member variables to 1
{
	height = width = length = 1;
}

//Constructor to intialize the fields of the box
Box::Box(double boxHeight, double boxWidth, double boxLength)
{
	setHeight(boxHeight);
	setWidth(boxWidth);
	setLength(boxLength);
}
/*********************************************************************
** Description: This method ensures that the height of the box is a
** non negative value.
** Parameters: double boxHeight
*********************************************************************/
void Box::setHeight(double boxHeight)
{
	if (boxHeight >= 0)
		height = boxHeight;
	else
	{
		height = 0;
		cout << "Height cannot be negative. \n";
	}
}
/*********************************************************************
** Description: This method ensures that the width of the box is a
** non negative value.
** Parameters: double boxWidth
*********************************************************************/
void Box::setWidth(double boxWidth)
{
	if (boxWidth >= 0)
		width = boxWidth;
	else
	{
		width = 0;
		cout << "Width cannot be negative. \n";
	}
}
/*********************************************************************
** Description: This method ensures that the length of the box is a
** non negative value.
** Parameters: double boxLength
*********************************************************************/
void Box::setLength(double boxLength)
{
	if (boxLength >= 0)
		length = boxLength;
	else
	{
		length = 0;
		cout << "Length cannot be negative. \n";
	}
}
/*********************************************************************
** Description: This method returns the volume of the box.
** Parameters: 
*********************************************************************/
double Box::getVolume()
{
	return height * width * length;
}
/*********************************************************************
** Description: This method returns the surface area of the box.
** Parameters: 
*********************************************************************/
double Box::getSurfaceArea()
{
	return (2 * length*width) + (2 * length*height) + (2 * width*height);
}

