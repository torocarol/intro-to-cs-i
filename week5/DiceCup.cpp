/*********************************************************************
** Author: Carol Toro
** Date: 2/3/2015
** Description: Assignment 5 - Exercise #1
** This program ask the users to enter the # of die they would like roll,
** how many faces the die have, and how many times they would like to roll
** the die. At the end it prints the sum of the values of the die
** after each roll.
*********************************************************************/
#include <iostream>
#include <cstdlib>
#include <ctime>
#include "DiceCup.hpp"
using namespace std;


int main()
{
	//Declare variables to take user input
	int cupDice,
		cupFaces,
		rolls,
		sumTotal = 0; //accumulator
	
	//Create random sequence based off time
	unsigned seed;
	seed = time(0);
	srand(seed);

	//Get # of dice, # faces, and # of desired rolls from user
	cout << "Enter # of dices: ";
	cin >> cupDice;
	cout << "Enter # of faces: ";
	cin >> cupFaces;
	cout << "How many times would you like to roll the dice? ";
	cin >> rolls;
	
	//Create DiceCup object
	DiceCup cup(cupDice, cupFaces);

	//Loop for each die roll & print sum of values of die for each roll
	for (int rollCount = 1; rollCount <= rolls; rollCount++)
	{
		sumTotal = cup.rollDice();
		cout << "The sum of the die for roll " << rollCount << " is " << sumTotal << endl;
	}
		
	return 0;
}

DiceCup::DiceCup()	//Default constructor to initialize the numDice and numFaces class member variables
{
	setNumDice(1);
	setNumFaces(2);
}
DiceCup::DiceCup(int diceNum, int facesNum) //Constructor to initialize the fields of the DiceCup
{
	setNumDice(diceNum);
	setNumFaces(facesNum);
}

/*********************************************************************
** Description: This method ensures that the # of die is a
** non negative value.
** Parameters: int diceNum
*********************************************************************/
void DiceCup::setNumDice(int diceNum)
{
	//Input validation
	if (diceNum >= 0)
		numDice = diceNum;
	else
	{
		numDice = 0;
		cout << "Number of dices is invalid. \n";
	}
	
}
/*********************************************************************
** Description: This method ensures that the # of faces of a dice is a
** non negative value.
** Parameters: int facesNum
*********************************************************************/
void DiceCup::setNumFaces(int facesNum)
{
	
	//Input validation
	if (facesNum >= 0)
		numFaces = facesNum;
	else
	{
		numFaces = 0;
		cout << "Number of faces is invalid. \n";
	}
	
}
/*********************************************************************
** Description: This method returns the sum of the value of each dice
** when all rolled at once.
** Parameters: none
*********************************************************************/
int DiceCup::rollDice()
{
	int dice,
		sum = 0;



	for (int diceCount = 1; diceCount <= numDice; diceCount++)
	{
		dice = rand() % numFaces + 1;
		sum += dice;
	}
	
	return sum;
	
}