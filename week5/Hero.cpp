/*********************************************************************
** Author: Carol Toro
** Date: 2/4/2015
** Description: Assignment 5 - Exercise #3
** This program asks the user to enter the amount of points they would like
** to assign to a hero's attributes (strength, hitpoints, speed, money).
** The program creates a hero object based off these inputs. Once
** the hero enters combat, the program asks the user to enter the amount
** of damage the hero receives until the hero's untimely death. Each time
** the hero receives damage, the program prints a status report of the hero's
** attributes.
*********************************************************************/
#include <iostream>
#include "Hero.hpp"
using namespace std;

int main()
{
	//Variables to store user inputs
	int userStrength,
		userHitPoints,
		userSpeed,
		userMoney,
		userDamage;

	//Ask & Get Hero fields from user
	cout << "How much strength does the hero have? ";
	cin >> userStrength;
	cout << "How many hitpoints does the hero have? ";
	cin >> userHitPoints;
	cout << "How much speed does the hero have? ";
	cin >> userSpeed;
	cout << "How much money does the hero have? ";
	cin >> userMoney;

	//Create hero object
	Hero hero1(userStrength, userHitPoints, userSpeed, userMoney);

	cout << "The hero has entered combat.\n";
	//Loop based on hero's health (alive or dead?)
	do
	{
		cout << "Enter the amount of damage the hero takes: ";
		cin >> userDamage;

		hero1.takeDamage(userDamage);
		cout << "*************************" << endl;
		cout << "Hero's strength: " << hero1.getStrength() << endl;
		cout << "Hero's hitpoints: " << hero1.getHitPoints() << endl;
		cout << "Hero's speed: " << hero1.getSpeed() << endl;
		cout << "Hero's money: " << hero1.getMoney() << endl;
		cout << "*************************" << endl;

	} while (hero1.takeDamage(0));
	
	cout << "\"Show me a hero and I'll write you a tragedy.\"- F. Scott Fitzgerald" << endl;
	cout << "Today the hero has met his tragedy." << endl;
		
}

Hero::Hero() //Default constructor
{
	strength = 1;
	hitPoints = 1;
	speed = 1;
	money = 1;
}

Hero::Hero(int heroStrength, int heroHitPoints, int heroSpeed, int heroMoney)
{
	setStrength(heroStrength);
	setHitPoints(heroHitPoints);
	setSpeed(heroSpeed);
	setMoney(heroMoney);
}

void Hero::setStrength(int heroStrength)
{
	strength = heroStrength;
}

void Hero::setHitPoints(int heroHitPoints)
{
	hitPoints = heroHitPoints;
}

void Hero::setSpeed(int heroSpeed)
{
	speed = heroSpeed;
}

void Hero::setMoney(int heroMoney)
{
	money = heroMoney;
}

int Hero::getStrength()
{
	return strength;
}

int Hero::getHitPoints()
{
	return hitPoints;
}

int Hero::getSpeed()
{
	return speed;
}

int Hero::getMoney()
{
	return money;
}

/*********************************************************************
** Description: This function returns true if the hero is still alive, 
** and reduces the hero's hitpoints by the damage it takes. 
** Parameters: int heroDamage
*********************************************************************/
bool Hero::takeDamage(int heroDamage)
{
	bool heroAlive;
	if (hitPoints <= 0)
	{
		return false;
	}
	else
	{
		int damage;
		damage = hitPoints - heroDamage;
		setHitPoints(damage);			
		return true;
	}
	return heroAlive;
}