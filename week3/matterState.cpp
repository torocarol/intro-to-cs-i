/*********************************************************************
** Author: Carol Toro
** Date: 1/19/2015
** Description: Assignment 3 - Exercise 1
** This program asks the user for a temperature and then calculates 
** four substances' physical state (solid, liquid, or gas) based on 
** that user defined temperature. The program then prints the state of 
** each substance at that temperature.
*********************************************************************/

#include <iostream>
using namespace std;

int main()
{
	int temp = 0;
	const int EAFP = -173,
		EABP = 172,
		MFP = -38,
		MBP = 676,
		OFP = -362,
		OBP = -306,
		WFP = 32,
		WBP = 212;

	//Get temperature from user
	cout << "Enter a temperature in Fahrenheit: ";
	cin >> temp;

	cout << "At a temperature of " << temp << " degrees Fahrenheit: \n";

	//Find the state for each substance
	//Ethyl Alcohol
	if ((temp > EAFP) && (temp < EABP))
		cout << "Ethyl Alcohol is a liquid. \n";
	else if (temp <= EAFP)
		cout << "Ethyl Alcohol is a solid. \n";
	else if (temp >= EABP)
		cout << "Ethly Alcohol is a gas. \n";

	//Mercury
	if ((temp > MFP) && (temp < MBP))
		cout << "Mercury is a liquid. \n";
	else if (temp <= MFP)
		cout << "Mercury is a solid. \n";
	else if (temp >= MBP)
		cout << "Mercury is a gas. \n";

	//Oxygen
	if ((temp > OFP) && (temp < OBP))
		cout << "Oxygen is a liquid. \n";
	else if (temp <= OFB)
		cout << "Oxygen is a solid. \n";
	else if (temp >= OBP)
		cout << "Oxygen is a gas. \n";

	//Water
	if ((temp > WFP) && (temp < WBP)
		cout << "Water is a liquid. \n";
	else if (temp <= WFP)
		cout << "Water is a solid. \n";
	else if (temp >= WBP)
		cout << "Water is a gas. \n";

	return 0;
}