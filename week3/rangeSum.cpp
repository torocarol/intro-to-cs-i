/*********************************************************************
** Author: Carol Toro
** Date: 1/20/2015
** Description: Assignment 3 - Exercise 2
** This program asks the user to enter two numbers. Based on those numbers,
** the program calculates the range of numbers in between and then
** calculates the total sum of all the numbers in that range.
*********************************************************************/
#include <iostream>
using namespace std;

int main()
{
	//Define variables
	int x,			//1st # input by user
		y,			//2nd # input by user
		range,		//
		upperBound,	//
		lowerBound,	//
		sum = 0.0;	//Accumulator, intialized with 0

	//Get input from user
	cout << "Enter two integers separated by a space: ";
	cin >> x >> y;

	//Determine bounds in order to calculate range
	if (x >= y)
	{
		upperBound = x;
		lowerBound = y;
	}
	else
	{
		upperBound = y;
		lowerBound = x;
	}
	
	//Calculate Range
	range = upperBound - lowerBound;

	//Get the numbers within range & accumulate a total
	for (int count = 1; count <= range; count++)
	{
		sum += lowerBound;
		lowerBound++;
	}

	//Display the total sum
	cout << "The sum of the range is " << (sum + upperBound ) << ".\n";
	return 0;
}