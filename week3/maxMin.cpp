/*********************************************************************
** Author: Carol Toro
** Date: 1/20/2015
** Description: Assignment 3 - Exercise 3
** This program asks the user how many numbers they would like to enter
** and lets the user enter that many values. The program then displays
** which of the user's values were the largest and the smallest.
*********************************************************************/
#include <iostream>
using namespace std;

int main()
{
	//Define variables
	int numNumbers,		//Number of numbers
		counter = 1,
		num = 1;
	double currentNum,	//Variable to hold current number
		max,			//Variable to hold largest number
		min;			//Variable to hold smallest number

	//Get the number of numbers from user
	cout << "How many numbers would you like to enter? ";
	cin >> numNumbers;

	//Read each number and set to max & min
	do
	{
		
		cout << "Enter number " << num++ << ": ";
		cin >> currentNum;
		while (counter <= numNumbers)
		{
			max = currentNum;
			min = currentNum;
			counter++;
		}
		//conditional to test for max & min
		if (currentNum >= max)
			max= currentNum;
		else if ( currentNum <= min)
			min = currentNum;
		

	} while (num <= numNumbers);
		
	cout << "The largest number is " << max << ".\n";
	cout << "The smallest number is " << min << ".\n";

	return 0;
}