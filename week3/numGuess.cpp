/*********************************************************************
** Author: Carol Toro
** Date: 1/22/2015
** Description: Assignment 3 - Exercise 4
** This program has a user guess a number randomly generated from 1
** to 100. After each guess the user makes, the program provides guidance
** on whether the user's guess was too high or too low. When the user 
** finally guesses the correct number, the program displays how many
** attempts it took the user to guess the number.
*********************************************************************/
#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
	//Define variables for program
	unsigned seed,
			answer, //variable to store the random # generated b/w 1&100
			guess;	//variable to store user's guess
	int tries = 0;	//variable to store # of attempts
	
	bool continueGame = true;

	//Generate random #
	seed = (unsigned int)time(0);
	srand(seed);

	//Assign random # to answer & limit from 1-100
	answer = rand() % 100 + 1;

	//Read each guess and keep tally of attempts
	do
	{
		cout << "Guess a random number between 1 and 100: ";
		cin >> guess;
		tries++;

		//Compare guess with random answer and provide feedback to user
		if (guess > answer)
		{
			cout << "The number you guessed, " << guess << ", is too high. Try again." << endl;
		}
		if (guess == answer)
		{
			cout << "Congratulations! You guessed the number in " << tries << " tries.";
			continueGame = false;
		}
		if (guess < answer)
		{
			cout << "The number you guessed, " << guess << ", is too low. Try again." << endl;
		}
	
	} while (continueGame);
	
	return 0;
}