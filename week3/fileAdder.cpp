/*********************************************************************
** Author: Carol Toro
** Date: 1/22/2015
** Description: Assignment 3 - Project
** This program opens a file num.txt which contains values. The program
** adds up all the values contained in num.txt and writes the sum of the
** values into a newly created file sum.txt. The program tests to make
** sure that both num.txt and sum.txt open properly.
*********************************************************************/
#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

int main()
{
	//Declare variables
	float number,	//variable used to hold number in each line
		sum=0;		//variable used to hold sum of each number	
	
	//Create FS object and attempt to open nums.txt file
	ifstream inputFile;
	inputFile.open("./nums.txt");
	
	//Test for opening of "nums.txt"
	if (inputFile.fail())
		cout << "The file nums.txt could not be opened.";
	
	else
	{
		//Loop until EOF reached
		while (inputFile >> number)
		{
			sum += number;
		}
		//close the file
		inputFile.close();

		//declare outputFile
		ofstream outputFile;

		//Open sum.text
		outputFile.open("./sum.txt");

		//Write data to output file
		outputFile << "The sum of the values is " << sum << ".\n";

		//Close the file
		outputFile.close();
	}

	//Open sum.txt and test for opening
	inputFile.open("./sum.txt");
	
	if (inputFile.fail())
		cout << "The file sum.txt could not be opened.";
	else
		inputFile.close();
	
	return 0;
}