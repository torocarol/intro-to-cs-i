/*********************************************************************
** Author: Carol D. Toro
** Date: 1/13/2015
** Description: Assignment 2 - Project 
** This program takes user input on loan amount, # of loan installments,
** and interest rate to calculate monthly loan payments, total amount paid,
** and total interest paid. At the end, it displays a summary of this
** information for the user to peruse.
*********************************************************************/
#include <iostream>
#include <iomanip>
#include <cmath>
using namespace std;

int main()
{
	int numPayments;
	double interestRate;
	double loan, monthlyPayment, amountPaid, interestPaid;

	//Get user input
	cout << "What is the amount of the loan? ";
	cin >> loan;
	cout << "How many monthly installments does the loan have? ";
	cin >> numPayments;
	cout << "What is the interest rate (%) of the loan? ";
	cin >> interestRate;

	// Calculate the monthly payments
	monthlyPayment = (((interestRate/100) * pow((1 + (interestRate/100)), numPayments)) / ((pow((1 + (interestRate/100)), numPayments) - 1)))*loan;

	// Calculate amount paid back
	amountPaid = monthlyPayment * numPayments;

	//Calculate interest paid
	interestPaid = amountPaid - loan;

	//Display Report
	cout << "Loan Amount:\t\t" << right  << fixed << showpoint << setprecision(2);
	cout << "$ " << setw(8) << loan << endl;
	//Get rid of fixed credit: http://www.cplusplus.com/reference/iomanip/resetiosflags/
	cout << resetiosflags(std::ios::fixed) << setprecision(3);
	cout << "Monthly Interest Rate:\t\t" << right << noshowpoint << interestRate << "%\n";
	cout << "Number of Payments:\t\t" << right << showpoint << numPayments << endl;
	cout << "Monthly Payment:\t" << right << fixed << showpoint << setprecision(2);
	cout << "$ " << setw(8) << monthlyPayment << endl;
	cout << "Amount Paid Back:\t" << right << "$ " << setw(8) << amountPaid << endl;
	cout << "Interest Paid:\t\t" << right << "$ " << setw(8) << interestPaid << endl;

	return 0;

}