/*********************************************************************
** Author: Carol Toro
** Date: 1/13/15
** Description: Assignment 2 - Exercise 4 
** This program converts celsius to fahrenheit by asking the user
** to input a temperature in celcius. The program then displays to 
** the user the conversion from celsius to fahrenheit
*********************************************************************/
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
	double celsius, fahrenheit;

	// Get user input
	cout << "Please enter a celsius temperature to convert: ";
	cin >> celsius;

	// Conversion
	fahrenheit = ((9.0/5) * celsius) + 32.0;

	//Display conversion to user
	cout << celsius << " degrees celsius converts to "  << fahrenheit << " degrees fahrenheit.\n";
	return 0;
}