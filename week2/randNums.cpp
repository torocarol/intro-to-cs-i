/*********************************************************************
** Author: Carol Toro
** Date: 1/13/2015
** Description: Assignment 2 - Exercise 1
** This program asks the user for a lower bound number and an upper
** bound number. It then generates 5 random numbers that fall within
** the user's lower and upper range.
*********************************************************************/
#include <iostream>
#include <cstdlib>
using namespace std;

int main()
{
	int upperBound, lowerBound, randNum;

	// Get lowerBound value from user
	cout << "Enter a lowerbound integer: ";
	cin >> lowerBound;
	// Get upperBound value from user
	cout << "Enter an upperbound integer: ";
	cin >> upperBound;

	//Display 5 random numbers within the user's range
	randNum = rand() % (upperBound - lowerBound + 1) + lowerBound;
	cout << randNum << endl;
	randNum = rand() % (upperBound - lowerBound + 1) + lowerBound;
	cout << randNum << endl;
	randNum = rand() % (upperBound - lowerBound + 1) + lowerBound;
	cout << randNum << endl;
	randNum = rand() % (upperBound - lowerBound + 1) + lowerBound;
	cout << randNum << endl;
	randNum = rand() % (upperBound - lowerBound + 1) + lowerBound;
	cout << randNum << endl;
}