/*********************************************************************
** Author: Carol Toro
** Date: 1/13/2015
** Description: Assignment 2 - Exercise 2 
** This program calculates the amount of income generated from Softball
** ticket sales by asking the user how many tickets were sold for the three
** types of available seats. The program displays the total sales generated
** as well as the sales generated by each seat type.
*********************************************************************/
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
	double aSold, bSold, cSold, total;
	int aSeat = 15;
	int bSeat = 12;
	int cSeat = 9;

	//Ask user for tickets sold
	cout << "How many tickets were sold for Class A seats? ";
	cin >> aSold;
	cout << "How many tickets were sold for Class B seats? ";
	cin >> bSold;
	cout << "How many tickets were sold for Class C seats? ";
	cin >> cSold;

	// Calculate total sold
	total = (aSold*aSeat) + (bSold*bSeat) + (cSold*cSeat);

	// Display total sold
	cout << "Ticket sales generated $" << fixed << showpoint;
	cout << setprecision(2) << total << " dollars. \n";
	cout << "Class A seats brought in $" << aSold*aSeat << " dollars.\n";
	cout << "Class B seats brought in $" << bSold*bSeat << " dollars.\n";
	cout << "Class C seats brought in $" << cSold*cSeat << " dollars.\n";

	return 0;
}