/*********************************************************************
** Author: Carol Toro
** Date: 02/09/15
** Description: This header file contains the member variable defitinions
** and member function definitions for the Point class.
*********************************************************************/
#ifndef POINT_HPP
#define POINT_HPP

class Point
{
private:
	double xCoord;
	double yCoord;
public:
	Point();
	Point(double, double);
	void setXCoord(double);
	void setYCoord(double);
	double getXCoord();
	double getYCoord();
	double distanceTo(const Point&);
};

#endif