/*********************************************************************
** Author: Carol Toro
** Date: 02/09/15
** Description: Assignment 6 - Exercise 1
** This program prompts the user for the year and make of a car and 
** uses this information to build a car object. The program then 
** displays a menu to the user with 3 options: accelarate car,
** brake car, or quit the program. When the user chooses to accelerate
** or brake the car, the program prints the current speed of the car.
** When the user chooses quit, the program ends.
*********************************************************************/

#include <iostream>
#include <string>
#include "Car.hpp"
using namespace std;

int main()
{
	//Variables to store user input
	int userYear;
	string userMake;
	int userChoice;

	//Initialize variables
	userYear = userChoice = 0;
	userMake = "";

	//Prompt user
	cout << "Enter the year of the car: ";
	cin >> userYear;
	cout << "Enter the make of the car: ";
	cin.get();
	getline(cin, userMake);

	//Create object
	Car userCar(userYear, userMake);

	//Loop for options
	

	do
	{
		//Display Menu & get user's choice
		cout << "\n What would you like to do with the car?\n";
		cout << "1. Accelerate the car\n";
		cout << "2. Hit the brakes on the car\n";
		cout << "3. Quit\n";
		cout << "Enter your choice (1-3): ";
		cin >> userChoice;

		if (userChoice != 3)
		{
			switch (userChoice)
			{
			case 1: userCar.accelerate();
				cout << "\n The current speed of the car is " << userCar.getSpeed() << " mph." << endl;
				break;
			case 2: userCar.brake();
				cout << "\n The current speed of the car is " << userCar.getSpeed() << " mph." << endl;
				break;
			}
		}

	} while (userChoice != 3);
	
		
	
	return 0;

}

Car::Car()
{
	year = 0;
	make = "";
	speed = 0;
}
Car::Car(int carYear, string carMake)
{
	setYear(carYear);
	setMake(carMake);
	setSpeed(0);
}
void Car::setYear(int carYear)
{
	year = carYear;
}
void Car::setMake(string carMake)
{
	make = carMake;
}
void Car::setSpeed(int carSpeed)
{
	speed = carSpeed;
}
int Car::getSpeed()
{
	return speed;
}

/*********************************************************************
** Description: This method increases the car's speed by a value of 10
** and notifies the user that the car's speed cannot go over 80
** Parameters: n/a
*********************************************************************/
void Car::accelerate()
{
	if (speed < 80)
	{
		int carSpeed;
		carSpeed = speed + 10;
		setSpeed(carSpeed);
	}
	else
		cout << "\n Car speed cannot be over 80. " << endl;
}
/*********************************************************************
** Description: This method decreases the car's speed by a value of 10
** and notifies the user that the car's speed cannot go below 0
** Parameters: n/a
*********************************************************************/
void Car::brake()
{
	if (speed >0)
	{
		int carBrake;
		carBrake = speed - 10;
		setSpeed(carBrake);
	}
	else
		cout << "\n Car speed cannot be below 0. " << endl;
}