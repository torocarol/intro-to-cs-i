/*********************************************************************
** Author: Carol Toro
** Date: 02/09/15
** Description: Assignment 6 - Project
** This program gets input�from�the�user�to�determine�the�coordinates�
** for�two�points.�With the user's input, the program creates two Point�
** objects and uses the objects to create a�LineSegment� object.�
** The program then prints out the LineSegment�object�s�length�and
** slope for the user.
*********************************************************************/

#include <iostream>
#include <cmath>
#include "LineSegment.hpp"
#include "Point.hpp"
using namespace std;

int main()
{
	//Variables to store user input
	double userXCoord1,
		userXCoord2,
		userYCoord1,
		userYCoord2,
		segmentLength,
		segmentSlope;
	
	//Initialization of variables
	userXCoord1 = userXCoord2 = userYCoord1 = userYCoord2 = segmentLength = segmentSlope = 0;

	//Ask and get coordinates from user
	cout << "Enter the x-coordinate for the first point: ";
	cin >> userXCoord1;
	cout << "Enter the y-coordinate for the first point: ";
	cin >> userYCoord1;
	cout << "Enter the x-coordinate for the second point: ";
	cin >> userXCoord2;
	cout << "Enter the y-coordinate for the second point: ";
	cin >> userYCoord2;

	//Create Point objects
	Point userPointOne(userXCoord1, userYCoord1);
	Point userPointTwo(userXCoord2, userYCoord2);

	
	//Create LineSegment object
	LineSegment userSegment(userPointOne, userPointTwo);

	segmentLength = userSegment.length();
	

	//Display Length
	cout << "The length is " << segmentLength << endl;

	segmentSlope = userSegment.slope();
	//Display Slope

	if (isinf(segmentSlope))
	{
		cout << "The line is vertical and the slope is infinity." << endl;
	}
	else
	{
		
		cout << "The slope of the line segment is " << segmentSlope << endl;
	}
	
}



LineSegment::LineSegment()
{
	Point defPointOne(0, 0);
	Point defPointTwo(0, 0);
	setEnd1(defPointOne);
	setEnd2(defPointTwo);

}
LineSegment::LineSegment(Point pointOne, Point pointTwo)
{
	setEnd1(pointOne);
	setEnd2(pointTwo);
}
void LineSegment::setEnd1(Point pointOne)
{
	endPoint1 = pointOne;
}
void LineSegment::setEnd2(Point pointTwo)
{
	endPoint2 = pointTwo;
}
Point LineSegment::getEnd1()
{
	return endPoint2;
}
Point LineSegment::getEnd2()
{
	return endPoint2;
}
/*********************************************************************
** Description: This method calls Point's member function distanceTo
** and passes endPoint 2 as a parameter.
** Parameters: n/a
*********************************************************************/
double LineSegment::length()
{
	return endPoint1.distanceTo(endPoint2);
}
/*********************************************************************
** Description: This method returns the slope between the two end points
** of a line segment.
** Parameters: n/a
*********************************************************************/
double LineSegment::slope()
{
	double yDiff = 0;
	double xDiff = 0;
	yDiff = endPoint2.getYCoord() - endPoint1.getYCoord();
	xDiff = endPoint2.getXCoord() - endPoint1.getXCoord();
	return (yDiff/xDiff);
		
}