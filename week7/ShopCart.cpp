/*********************************************************************
** Author: Carol Toro
** Date: 02/19/15
** Description: Assignment 7 - Exercise 2
** This program prompts the user with 4 options for a shopping cart:
** the user can add items to their shopping cart, view the contents
** of their shopping cart, get the total price of the items in their
** cart, and or quit the program.
*********************************************************************/
#include "Item.hpp"
#include "ShopCart.hpp"
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cstring>
using namespace std;

int main()
{
	//Define and initialize variables
	string userItemName = "",
		userItemPrice = "",
		userItemQuantity = "";
	int userChoice = 0,
		userQuantity = 0;
	double userPrice = 0.0,
		userTotalCost = 0.0;
	bool priceValid = true,
		quantityValid = true;


	//Create user cart
	ShoppingCart userCart;


	//Display Menu
	do
	{
		//Display Menu & get user's choice
		cout << "\n Please choose from the menu options below: \n";
		cout << "1. Add an item to your shopping cart \n";
		cout << "2. List the total contents of your shopping cart \n";
		cout << "3. Get the total price of the contents of your shopping cart \n";
		cout << "4. Quit \n";
		cout << "Enter your choice (1-4): ";
		cin >> userChoice;
		cin.ignore();

		if (userChoice == 1)
		{
			 //Get Item info from user
			cout << "Please enter the name of the Item you would like to add to the cart: ";
			getline(cin, userItemName);
			
			
			//Get item price & validate
			while (priceValid)
			{
				cout << "Please enter the price of the Item: ";
				getline(cin, userItemPrice);

				// Check to make sure each character is a digit or has a period
				bool charValid = true;
				int strLength = userItemPrice.length();
				for (int count = 0; count < strLength; count++)
				{
					if (!isdigit(userItemPrice[count]) && (userItemPrice[count] != '.'))
					{
							charValid = false;
							break;

					}
				}
				if (!charValid)
					continue;
				break;
			}
			/*convert price string to c-string then convert to double */
			char *price_string = new char[userItemPrice.length() + 1];
			// Copy string into c-string.
			strcpy(price_string, userItemPrice.c_str());
			// Convert c-string to double
			userPrice = atof(price_string);
				
			//Get Quantity & Validate
			while (quantityValid)
			{
				cout << "Please enter the quantity of the item: ";
				getline(cin, userItemQuantity);

				// Check to make sure each character is a digit
				bool quantValid = true;
				int strLength = userItemQuantity.length();
				for (int count = 0; count < strLength; count++)
				{
					if (!isdigit(userItemQuantity[count]))
					{
						quantValid = false;
						break;
					}
				}
				if (!quantValid)
					continue;
				break;
			}
			/*convert quantity string to c-string then convert to int */
			char *quantity_string = new char[userItemQuantity.length() + 1];
			// Copy string into c-string.
			strcpy(quantity_string, userItemQuantity.c_str());
			// Convert c-string to double
			userQuantity = atoi(quantity_string);

			
			//Create userItem
			Item userItem(userItemName, userPrice, userQuantity);

			//add useritem to cart
			userCart.addItem(userItem);
		}
	
		if (userChoice==2)
		{
			userCart.listContents();
			
		}

		if (userChoice == 3)
		{
			userTotalCost = userCart.totalPrice();
			cout << "Total price of items in cart is $" << fixed << setprecision(2) << userTotalCost << endl;
		}

	} while (userChoice != 4);
	return 0;


}
/*********************************************************************
** Description: This method takes in an Item and adds it to the
** shopping cart.
** Parameters: Item userProduct
*********************************************************************/
void ShoppingCart::addItem(Item userProduct)
{
	cart.push_back(userProduct);
}
/*********************************************************************
** Description: This method lists all of the contents in the shopping
** cart.
** Parameters: n/a
*********************************************************************/
void ShoppingCart::listContents()
{
	int itemsInCart = 0;
	itemsInCart = cart.size();
	//loop to print out the items in the cart
	for (int count = 0; count < itemsInCart; count++)
	{
		cout << fixed << setprecision(2) << endl;
		cout << "Name: " << cart[count].getName() << endl;
		cout << "Price: " << cart[count].getPrice() << endl;
		cout << "Quantity: " << cart[count].getQuantity() << endl;
	}
	cout << endl;

}
/*********************************************************************
** Description: This method lists the total price of all of the items
** in the shopping cart.
** Parameters: n/a
*********************************************************************/
double ShoppingCart::totalPrice()
{
	int itemsInCart = 0;
	itemsInCart = cart.size();
	double totalCartPrice = 0;

	//loop to get the total cost of the items in the cart
	for (int count = 0; count < itemsInCart; count++)
	{
		double itemPrice = 0;
		itemPrice = cart[count].getPrice() * cart[count].getQuantity();
		totalCartPrice += itemPrice;
	}
	
	return totalCartPrice;

}