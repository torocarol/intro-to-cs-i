/*********************************************************************
** Author: Carol Toro
** Date: 02/21/15
** Description:This class specification file contains the class 
** definitions for the TicTacToe class.
*********************************************************************/
#ifndef TICTACTOE_HPP
#define TICTACTOE_HPP
#include "Board.hpp"

class TicTacToe
{
private:
	Board gameBoard;
	char playerTurn;
public:
	TicTacToe(char);
	void setPlayerTurn(char);
	void play();

};

#endif