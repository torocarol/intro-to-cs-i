/*********************************************************************
** Author: Carol Toro
** Date: 02/19/15
** Description: This file contains the definitions of the Item class 
** member functions.
*********************************************************************/
#include "Item.hpp"
#include <string>
using namespace std;

Item::Item() //Default constructor
{
	name = "";
	price = 0.0;
	quantity = 0;
}
Item::Item(string itemName, double itemPrice, int itemQuantity)
{
	setName(itemName);
	setPrice(itemPrice);
	setQuantity(itemQuantity);
}
void Item::setName(string itemName)
{
	name = itemName;
}
void Item::setPrice(double itemPrice)
{
	price = itemPrice;
}
void Item::setQuantity(int itemQuantity)
{
	quantity = itemQuantity;
}
string Item::getName()
{
	return name;
}
double Item::getPrice()
{
	return price;
}
int Item::getQuantity()
{
	return quantity;
}
