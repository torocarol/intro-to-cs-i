/*********************************************************************
** Author: Carol Toro
** Date: 02/19/15
** Description: This header file contains the member variable defitinions
** and member function definitions for the Item class.
*********************************************************************/
#ifndef ITEM_HPP
#define ITEM_HPP
#include <string>
using namespace std;

class Item
{
private:
	string name;
	double price;
	int quantity;
public:
	Item();
	Item(string, double, int);
	void setName(string);
	void setPrice(double);
	void setQuantity(int);
	string getName();
	double getPrice();
	int getQuantity();
};

#endif