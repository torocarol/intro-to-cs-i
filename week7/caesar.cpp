/*********************************************************************
** Author: Carol Toro
** Date: 02/19/15
** Description: Assignment 7 - Exercise 1
** This program prompts acts like a Caesar enchiper and prompts the 
** user for a message and the amount of offset they would like. 
** The program then enciphers the user's message and prints out the
** enciphered form of the message.
*********************************************************************/
#include <iostream>
#include <cctype>
#include <cstring>
using namespace std;

void encipher(char[], int); //function prototype

int main()
{
	//define & initialize variables
	const int SIZE = 100;
	char userString[SIZE]="";
	int userOffset = 0;

	//get user input
	cout << "Enter a message less than 100 characters and I will encipher it for you: ";
	cin.getline(userString, SIZE);
	cout << "Please enter the offset of the enciphering: ";
	cin >> userOffset;

	//call function
	encipher(userString, userOffset);

	return 0;
}

/*********************************************************************
** Description: This function enciphers each character in the message
** based on the user's desired offset. This function also prints out
** the enciphered form of the message.
** Parameters: char, and int
*********************************************************************/
void encipher(char userPhrase[], int offset)
{
	int strLength;
	strLength = strlen(userPhrase);

	cout << "Here is the enchiphered form of your message: " << endl;
	//Loop for each character in the string
	for (int index = 0; index < strLength; index++)
	{
		//Make changes to characters in string as long as they are alpha-numeric
		if (isalpha(userPhrase[index]))
		{

			if (isupper(userPhrase[index]))
			{
				//Make appropriate offset changes to capitalized letters
				userPhrase[index] = (((userPhrase[index] - 65) + offset) % 26) + 65;
			}
			else
			{
				//Make appropriate offset changes to lowercase letters
				userPhrase[index] = (((userPhrase[index] - 97) + offset) % 26) + 97;
			}

		}
		//Print the message
		cout << userPhrase[index];
	}
	cout << endl;
}