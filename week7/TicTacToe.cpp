/*********************************************************************
** Author: Carol Toro
** Date: 02/21/15
** Description: Assignment 7 - Project
** This class implementation file contains the member 
** function definitions for the TicTacToe class and also contains
** the client program. This program asks the user whether X or O will
** go first in tic-tac-toe. The program then calls a function to start 
** a game of tic-tac-toe between two players. The program continues
** until someone has won the game, or the game is a draw.
*********************************************************************/
#include "Board.hpp"
#include "TicTacToe.hpp"
#include <iostream>
#include <iomanip>
#include <cctype>
using namespace std;

int main()
{
	
	char userInput; //define and initialize variable 
	userInput = ' '; //to store which player will go first

	//Prompt user which player should go first
	cout << "Which user should go first in tic-tac-toe? (X or O)";
	cin >> userInput;

	//Create tictactoe object
	TicTacToe playerBoard(userInput);

	//Calling play & starting the game
	playerBoard.play();
	
	return 0;
}

/*********************************************************************
** Constructor takes in which player will go first and sets it as the
** current player.
*********************************************************************/
TicTacToe::TicTacToe(char userTurn)
{
	setPlayerTurn((toupper(userTurn)));
	
}

/*********************************************************************
** Set method sets which player's turn it is.
*********************************************************************/
void TicTacToe::setPlayerTurn(char userTurn)
{
	playerTurn = userTurn;
}
/*********************************************************************
** Description: This method keeps a looping asking the correct player
** for their move, sends the move to the board, and continues until
** someone has won, or the game is a draw.
** Parameters: N/A
*********************************************************************/
void TicTacToe::play()
{ 
	//define and intialize variables to use
	int xCoord = 0;
	int yCoord = 0;
	bool coordValid = true;

	// loop to repeat while game is in progress
	do {
		gameBoard.print();
		cout << "\n \n Player " << playerTurn << ", it's your turn. Please enter ";
		cout << "\n the coordinates of your move separated by a space: ";
		cin >> yCoord >> xCoord;

		//Call makeMove
		coordValid = gameBoard.makeMove(yCoord, xCoord, playerTurn);
		while (!coordValid)
		{
			cout << "\n Invalid move. That space is taken. Please enter ";
			cout << "\n the coordinates of your move separated by a space: ";
			cin >> yCoord >> xCoord;

			coordValid = gameBoard.makeMove(yCoord, xCoord, playerTurn);
		}

		//switch players
		if (playerTurn == 'X')
		{
			setPlayerTurn('O');
		}
		else if (playerTurn == 'O')
		{
			setPlayerTurn('X');
		}

	} while ((gameBoard.gameState() == P) && (gameBoard.gameState() != X) && (gameBoard.gameState() != O) && (gameBoard.gameState() != D));

	//Outcome of the game
	if (gameBoard.gameState() == X)
	{
		cout << "\n X has won the game.\n" << endl; 
		gameBoard.print();
	}
	else if (gameBoard.gameState() == O)
	{
		cout << "\n O has won the game.\n" << endl;
		gameBoard.print();
	}
	else if (gameBoard.gameState() == D)
	{
		cout << "\n The game is a draw.\n" << endl;
		gameBoard.print();
	}

}