/*********************************************************************
** Author: Carol Toro
** Date: 02/19/15
** Description: This header file contains the member variable defitinions
** and member function definitions for the ShoppingCart class.
*********************************************************************/
#ifndef SHOPCART_HPP
#define SHOPCART_HPP
#include <vector>
#include <string>
#include "Item.hpp"

class ShoppingCart{
private:
	vector<Item> cart;
public:
	void addItem(Item);
	void listContents();
	double totalPrice();
};
#endif