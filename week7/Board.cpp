/*********************************************************************
** Author: Carol Toro
** Date: 02/21/15
** Description: This class implementation file contains the member
** function definitions for the Board class.
*********************************************************************/
#include "Board.hpp"
#include <iostream>
#include <iomanip>
using namespace std;


Board::Board() //Default constructor to intialize board spaces with '.'
{
	for (int indexRow = 0; indexRow < NUM_ROWS; indexRow++)
	{
		for (int indexCol = 0; indexCol<NUM_COLS; indexCol++)
			gameBoard[indexRow][indexCol] = '.';
	}
		
}
/*********************************************************************
** Description: This method returns true when a player makes a valid move
** and records their move to the board; otherwise, it returns false and
** does not record their move to the board.
** Parameters: int yCoord, int xCoord, char playerTurn
*********************************************************************/
bool Board::makeMove(int yCoord, int xCoord, char playerTurn)
{

	if (gameBoard[yCoord][xCoord] == '.') 
	{
		gameBoard[yCoord][xCoord] = playerTurn;

		return true;
	}
	else 
		return false;
}
/*********************************************************************
** Set method: this method sets the current state of the game to
** status which contains an ENUM.
*********************************************************************/
void Board::setStatus(State sState)
{
	status = sState;
}
/*********************************************************************
** Description: This method reveals the state of the game, whether X
** or O has won, the game is in progress, or the game is a draw.
** Parameters: n/a
*********************************************************************/
State Board::gameState()
{
	bool fullBoard = true; // boolean variable to determine whether board is full

	//X or O have 3 in the first row
	if ((gameBoard[0][0] == 'X') && (gameBoard[0][1] == 'X') && (gameBoard[0][2] == 'X'))
	{
		setStatus(X);
		return status;
	}
	else if ((gameBoard[0][0] == 'O') && (gameBoard[0][1] == 'O') && (gameBoard[0][2] == 'O'))
	{
		setStatus(O);
		return status;
	}
	//X or O have 3 in the middle row
	else if ((gameBoard[1][0] == 'X') && (gameBoard[1][1] == 'X') && (gameBoard[1][2] == 'X'))
	{
		setStatus(X);
		return status;
	}
	else if ((gameBoard[1][0] == 'O') && (gameBoard[1][1] == 'O') && (gameBoard[1][2] == 'O'))
	{
		setStatus(O);
		return status;
	}
	//X or O have 3 in the last row
	else if ((gameBoard[2][0] == 'X') && (gameBoard[2][1] == 'X') && (gameBoard[2][2] == 'X'))
	{
		setStatus(X);
		return status;
	}
	else if ((gameBoard[2][0] == 'O') && (gameBoard[2][1] == 'O') && (gameBoard[2][2] == 'O'))
	{
		setStatus(O);
		return status;
	}
	//X or O have 3 in the 1st column
	else if ((gameBoard[0][0] == 'X') && (gameBoard[1][0] == 'X') && (gameBoard[2][0] == 'X'))
	{
		setStatus(X);
		return status;
	}
	else if ((gameBoard[0][0] == 'O') && (gameBoard[1][0] == 'O') && (gameBoard[2][0] == 'O'))
	{
		setStatus(O); 
		return status;
	}
	//X or O have 3 in the middle column
	else if ((gameBoard[0][1] == 'X') && (gameBoard[1][1] == 'X') && (gameBoard[2][1] == 'X'))
	{
		setStatus(X);
		return status;
	}
	else if ((gameBoard[0][1] == 'O') && (gameBoard[1][1] == 'O') && (gameBoard[2][1] == 'O'))
	{
		setStatus(O);
		return status;
	}
	//X or O have 3 in the last column
	else if ((gameBoard[0][2] == 'X') && (gameBoard[1][2] == 'X') && (gameBoard[2][2] == 'X'))
	{
		setStatus(X);
		return status;
	}
	else if ((gameBoard[0][2] == 'O') && (gameBoard[1][2] == 'O') && (gameBoard[2][2] == 'O'))
	{
		setStatus(O);
		return status;
	}
	//X or O have diagonals
	else if ((gameBoard[0][0] == 'X') && (gameBoard[1][1] == 'X') && (gameBoard[2][2] == 'X'))
	{
		setStatus(X);
		return status;
	}
	else if ((gameBoard[0][0] == 'O') && (gameBoard[1][1] == 'O') && (gameBoard[2][2] == 'O'))
	{
		setStatus(O);
		return status;
	}
	else if ((gameBoard[2][0] == 'X') && (gameBoard[1][1] == 'X') && (gameBoard[0][2] == 'X'))
	{
		setStatus(X);
		return status;
	}
	else if ((gameBoard[2][0] == 'O') && (gameBoard[1][1] == 'O') && (gameBoard[0][2] == 'O'))
	{
		setStatus(O);
		return status;

	}

	//Game is in progress
	for (int row = 0; row < NUM_ROWS; row++)
	{
		for (int col = 0; col < NUM_COLS; col++)
		{
			if (gameBoard[row][col] == '.')
			{
				setStatus(P);
				return status;
				break;
			}
		}
	}

	//Check to see if board is full Game, if board is full it will be a draw
	for (int row = 0; row < NUM_ROWS; row++)
	{
		for (int col = 0; col < NUM_COLS; col++)
		{
			if (gameBoard[row][col] == 'X')
			{
				fullBoard = true;
			}
			else if (gameBoard[row][col] == 'O')
			{
				fullBoard = true;
			}
			else
			{
				fullBoard = false;
			}
		}
	}
	//Game is a draw
	if (fullBoard)
	{
		setStatus(D);
		return status;
	}
	return status;
}
/*********************************************************************
** Description: This method prints the current state of the game board.
** Parameters: N/A
*********************************************************************/
void Board::print()
{
	//Show number for each column
	cout << setw(8) << " 0 " << setw(6) << " 1 " << setw(6) << " 2 " << endl;
	
	//Loop to print each row and each column of the board
	for (int row = 0; row < NUM_ROWS; row++)
	{
		cout << row << " "; //Show number for each row
		for (int col = 0; col < NUM_COLS; col++)
		{	
			cout << setw(5) << gameBoard[row][col] << " ";
		}
		cout << endl;
	}
}