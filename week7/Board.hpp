/*********************************************************************
** Author: Carol Toro
** Date: 02/21/15
** Description: This class specification file contains the class 
** definitions for the Board class.
*********************************************************************/
#ifndef BOARD_HPP
#define BOARD_HPP

const int NUM_COLS = 3;
const int NUM_ROWS = 3;
enum State { X, O, D, P };

class Board
{
private:
	char gameBoard[NUM_ROWS][NUM_COLS];
	State status;
public: 
	Board();
	bool makeMove(int, int, char);
	void setStatus(State);
	State gameState();
	void print();
};

#endif