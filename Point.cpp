/*********************************************************************
** Author: Carol Toro
** Date: 02/09/15
** Description: This file contains the definitions of the Point class 
** member functions.
*********************************************************************/
#include "Point.hpp"
#include <cmath>
#include <iostream>
#include <iomanip>
using namespace std;



Point::Point()
{
	xCoord = 0;
	yCoord = 0;
}
Point::Point(double x, double y)
{
	setXCoord(x);
	setYCoord(y);
}
void Point::setXCoord(double x)
{
	xCoord = x;
}
void Point::setYCoord(double y)
{
	yCoord = y;
}

double Point::getXCoord()
{
	return xCoord;
}

double Point::getYCoord()
{
	return yCoord;
}

/*********************************************************************
** Description: This method calculates and returns the distance 
** between�the�an object�s�point being called and the point�passed�
** in�the�parameter� 
** Parameters: const Point pointOne
*********************************************************************/
double Point::distanceTo(const Point &pointOne)
{
	double distance = 0;
	double xDistance = 0;
	double yDistance = 0;
	xDistance = this->getXCoord() - pointOne.xCoord; 
	yDistance = this->getYCoord() - pointOne.yCoord;
	distance = sqrt((xDistance*xDistance) + (yDistance*yDistance));
		
	return distance;
}