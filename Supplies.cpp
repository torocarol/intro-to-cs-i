/*********************************************************************
** Author: Carol Toro	
** Date: 02/13/15
** Description: Assignment 6 - Exercise 2
** This program prompts the user for the current stock of parts a factory
** has and creates a Supplies object with the user's input. The program
** then presents the user with 4 options: 2 options to produce two products:
** Thing1 and Thing2, a 3rd option to print out how many factory parts
** need to be ordered, and the 4th option to quit the program. When the 
** user chooses to produce a Thing1 or Thing2, the program prompts the user
** with how many they would like to make. 
*********************************************************************/
#include "Supplies.hpp"
#include <iostream>
using namespace std;

int main()
{
	int userThings, //Declare variable to hold user input
		userWatchas,
		userFramis,
		userChoice,
		userThing1,
		userThing2;
	
	//initialize variables
	userThings = userWatchas = userFramis = userChoice = userThing1 = userThing2 = 0;

	//Prompt user for current stock
	cout << "How many thingamajigs are there in stock? ";
	cin >> userThings;
	cout << "How many watchamacallits are there in stock? ";
	cin >> userWatchas;
	cout << "How many framistats are there in stock? ";
	cin >> userFramis;
	
	//Create object
	Supplies userSupplies(userThings, userWatchas, userFramis);

	//Loop for options

	do
	{
		//Display Menu & get user's choice
		cout << "\n Please choose from the options below: \n";
		cout << "1. Produce a thing1 \n";
		cout << "2. Produce a thing2 \n";
		cout << "3. Print a parts order \n";
		cout << "4. Quit \n";
		cout << "Enter your choice (1-4): ";
		cin >> userChoice;

		if (userChoice != 4)
		{
			switch (userChoice)
			{
			case 1: cout << "How many Thing1s do you want to make? ";
				cin >> userThing1;
				userSupplies.produceThing1(userThing1);
				break;
			case 2: cout << "How many Thing2s do you want to make? ";
				cin >> userThing2;
				userSupplies.produceThing2(userThing2);
				break;
			case 3: userSupplies.printPartsOrder();
				break;
			}
		}

	} while (userChoice != 4);

	return 0;
}

Supplies::Supplies()
{
	setNumThingamajigs(100);
	setNumWatchamacallits(70);
	setNumFramistats(50);
}

Supplies::Supplies(int numThing, int numWatcha, int numFrami)
{
	setNumThingamajigs(numThing);
	setNumWatchamacallits(numWatcha);
	setNumFramistats(numFrami);
}

void Supplies::setNumThingamajigs(int numThings)
{
	thingamajigs = numThings;
}

void Supplies::setNumWatchamacallits(int numWatchas)
{
	watchamacallits = numWatchas;
}

void Supplies::setNumFramistats(int numFramis)
{
	framistats = numFramis;
}
/*********************************************************************
** Description: This method prints the amount of parts that need to be
** ordered to restock the factory to the default amounts.
** Parameters: n/a
*********************************************************************/
void Supplies::printPartsOrder()
{
	//Variables to hold default amounts
	const int THINGAMAJIGS = 100,
		WATCHAMACALLITS = 70,
		FRAMISTATS = 50;

	int needThings, //Declare variables to hold amount needed to bring
		needWatchas, //stock up to default amounts
		needFramis;
	needThings = needWatchas = needFramis = 0; //initialize variables

	//Calculate amount needed to order
	needThings = THINGAMAJIGS - thingamajigs;
	needWatchas = WATCHAMACALLITS - watchamacallits;
	needFramis = FRAMISTATS - framistats;

	//Print Parts Order
	cout << "You will need to order the following amounts: " << endl;
	cout << needThings << " thingamajigs." << endl;
	cout << needWatchas << " watchamacallits." << endl;
	cout << needFramis << " framistats." << endl;
}
/*********************************************************************
** Description: This method takes as a paramater the number of Thing1s
** to produce and updates the value of parts in stock.
** Parameters: int numThing1
*********************************************************************/
void Supplies::produceThing1(int numThing1)
{
	int usedThings, //Declare variables to hold value of parts
		usedWatchas, //used to product Thing1
		usedFramis;
	usedThings = usedWatchas = usedFramis = 0; //intialize variables

	//Calculate amounts used
	usedThings = numThing1 * 1;
	usedWatchas = numThing1 * 2;
	usedFramis = numThing1 * 1;

	//Calculate parts should be in stock
	usedThings = thingamajigs - usedThings;
	usedWatchas = watchamacallits - usedWatchas;
	usedFramis = framistats - usedFramis;

	//Update parts in stock
	setNumThingamajigs(usedThings);
	setNumWatchamacallits(usedWatchas);
	setNumFramistats(usedFramis);
}

/*********************************************************************
** Description: This method takes as a paramater the number of Thing2s
** to produce and updates the value of parts in stock.
** Parameters: int numThing2
*********************************************************************/
void Supplies::produceThing2(int numThing2)
{
	int usedThings, //Declare variables to hold value of parts
		usedFramis;  //used to product Thing1
	usedThings = usedFramis = 0; //intialize variables

	//Calculate amounts used
	usedThings = numThing2 * 2;
	usedFramis = numThing2 * 1;

	//Calculate parts should be in stock
	usedThings = thingamajigs - usedThings;
	usedFramis = framistats - usedFramis;

	//Update parts in stock
	setNumThingamajigs(usedThings);
	setNumFramistats(usedFramis);
}
