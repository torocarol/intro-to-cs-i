/*********************************************************************
** Author: Carol Toro
** Date: 02/09/15
** Description: This header file contains the member variable defitinions
** and member function definitions for the LineSegment class.
*********************************************************************/
#ifndef LINESEGMENT_HPP
#define LINESEGMENT_HPP
#include "Point.hpp"


class LineSegment
{
private:
	Point endPoint1;
	Point endPoint2;
public:
	LineSegment();
	LineSegment(Point, Point);
	void setEnd1(Point);
	void setEnd2(Point);
	Point getEnd1();
	Point getEnd2();
	double length();
	double slope();
};

#endif