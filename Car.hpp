/*********************************************************************
** Author: Carol Toro
** Date: 02/09/15
** Description: This header file contains the member variable defitinions
** and member function definitions for the Car class.
*********************************************************************/
#ifndef CAR_HPP
#define CAR_HPP
#include <string>
using namespace std;

class Car
{
private:
	int year;
	string make;
	int speed;
public:
	Car();
	Car(int, string);
	void setYear(int);
	void setMake(string);
	void setSpeed(int);
	int getSpeed();
	void accelerate();
	void brake();
};


#endif