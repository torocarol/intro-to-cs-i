/*********************************************************************
** Author: Carol Toro
** Date: 1/27/15
** Description: Assignment 4 - Exercise #3
** This program prompts the user for a time value in seconds and then
** tells the user the distance an object has fallen in that time.
*********************************************************************/
#include <iostream>
using namespace std;

double fallDistance(double time);

int main()
{
	//Declare variables
	double userTime, //variable to hold seconds
		distance; //variable to hold distance an object has fallen


	//Get value from user
	cout << "Enter a falling time in seconds, and I will return the distance " << endl;
	cout << "in meters that an object has fallen: ";
	cin >> userTime;

	//Call function to get distance
	distance = fallDistance(userTime);

	//Display distance
	cout << "The object has fallen " << distance << " meters in " << userTime << " seconds.\n";
	
	/************************************************************************************************
	Another way I could have done this is by calling the function with cout, do you have a preference?
	cout << "The object has fallen " << fallDistance(userTime) << " meters\n";
	************************************************************************************************/

	return 0;
}
/*********************************************************************
** Description: This function takes in a double value in seconds and
** returns the distance in meters an object has fallen during that time.
** Parameters: double time
*********************************************************************/
double fallDistance(double time)
{
	return 0.5 * 9.8 * (time * time);
}