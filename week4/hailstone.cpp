/*********************************************************************
** Author: Carol Toro
** Date: 1/27/15
** Description: Assignment 4 - Project
** This program prompts a user for a positive integer and then calculates
** how many steps in a hailstone sequence it takes to reach 1.
*********************************************************************/
#include <iostream>
using namespace std;

int hailstone(int num);
int main()
{
	//Declare variable
	int userNum;

	//Get input from user
	cout << "Enter a postive integer: ";
	cin >> userNum;


	//Call Hailstone function & Print hailstone result to console
	cout << "For the number " << userNum << ", it takes " << hailstone(userNum) << " steps to return 1.\n";

}

/*********************************************************************
** Description: This function goes through the hailstone sequence of a
** number and returns how many steps it takes for the sequence to
** reach the number 1.
** Parameters: int num
*********************************************************************/
int hailstone(int num)
{
	//Set counter to 0
	int counter = 0;

	//While loop current num != 1
	while (num != 1)
	{
		//if num is even
		if (num % 2 == 0)
		{
			//set current num to num/2
			num = num / 2;
		}
		else
		{
			//set current num to (num*3)+1
			num = (num*3) + 1;
			
		}
		//set counter to counter+1
		counter++;
	}

	//return value of counter
	return counter;
}