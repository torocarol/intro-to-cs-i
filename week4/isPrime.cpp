/*********************************************************************
** Author: Carol Toro
** Date: 1/26/15
** Description: Assignment 4 - Exercise #2
** This program gets an integer number from the user, calls a function 
** to find out if the user's number is prime, and then tells the user
** whether the number is prime or not.
*********************************************************************/
#include <iostream>
using namespace std;

bool isPrime(int num);

int main()
{
	int userNum; // Variable to store user's number

	//Get number from user
	cout << "Enter an integer value and I will tell you if it is prime: ";
	cin >> userNum;

	//Call function & display if prime or not
	if (isPrime(userNum))
		cout << "The number is prime.\n";
	else
		cout << "The number is not prime.\n";

	return 0;

}

/*********************************************************************
** Description: This function takes an integer parameter and returns 
** true if that integer is prime, otherwise it returns false.
** Parameters: int num
*********************************************************************/
bool isPrime(int num)
{
	//For loop to return false when num is divisible by other number than 1 & itself
	for (int a = 2; a < num; a++)
	{
		if (num % a == 0)
		{
			return false;
		}
		
	} //End of loop
	return true;

}