/*********************************************************************
** Author: Carol Toro
** Date: 1/26/15
** Description: Assignment 4 - Exercise #1
** This program gets 3 integer values from the user, prints the 
** user's 3 values, calls a function to sort the 3 values in ascending
** order, and finally prints the 3 sorted values.
*********************************************************************/
#include <iostream>
using namespace std;

void sortNums(int&, int&, int&);

int main()
{
	//Declare variables
	int num1, num2, num3;

	//Prompt user for 3 int values
	cout << "Enter 3 integer values separated by a space: ";
	cin >> num1 >> num2 >> num3;

	//Print user's values
	cout << "This is the original order of your values: \n";
	cout << num1 << endl;
	cout << num2 << endl;
	cout << num3 << endl;

	//Call function to sort values
	sortNums(num1, num2, num3);

	//Print the sorted values
	cout << "This is the sorted order of your values: \n";
	cout << num1 << endl;
	cout << num2 << endl;
	cout << num3 << endl;

	return 0;
}

/*********************************************************************
** Description: This function takes in 3 int arguments passed by reference
** and sorts them in ascending order. 
** Parameters: &sortNum1, &sortNum2, &sortNum3
*********************************************************************/
void sortNums(int &sortNum1, int &sortNum2, int &sortNum3)
{
	int temp;
	
	//Check to see if num1 is greater than both num3 and num2
	if ((sortNum1 > sortNum2) && (sortNum1 > sortNum3))
	{
		temp = sortNum1;
		sortNum1 = sortNum2;
		sortNum2 = sortNum3;
		sortNum3 = temp;
		if (sortNum1 > sortNum2)
		{
			temp = sortNum1;
			sortNum1 = sortNum2;
			sortNum2 = temp;
		}
	}
	//Check to see if num2 is greater than both num3 and num1
	else if ((sortNum2 > sortNum1) && (sortNum2 > sortNum3))
	{
		temp = sortNum3;
		sortNum3 = sortNum2;
		sortNum2 = temp;
				
		if (sortNum1 > sortNum2)
		{
			temp = sortNum1;
			sortNum1 = sortNum2;
			sortNum2 = temp;

		}
	}

	//Check to see if num3 is greater than both num1 and num2
	else if ((sortNum3 > sortNum1) && (sortNum3 > sortNum2))
	{
		if (sortNum1 > sortNum2)
		{
			temp = sortNum1;
			sortNum1 = sortNum2;
			sortNum2 = temp;

		}
	}
}