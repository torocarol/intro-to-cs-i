/*********************************************************************
** Author: Carol Toro
** Date: 1/27/15
** Description: Assignment 4 - Exercise #4
** This program prompts a user to enter a string and then tells them
** if the string is a palindrome or not.
*********************************************************************/
#include <iostream>
#include <string>
using namespace std;

bool isPalindrome(string word);
int main()
{
	//declare variables
	string userString;

	//Get string from user
	cout << "Enter a string and I will let you know if it is a palindrome: ";
	cin >> userString; 

	//Call function to validate if palindrome & display if palindrome or not
	
	if (isPalindrome(userString))
		cout << userString << " is a palindrome.\n";
	else
		cout << userString << " is not a palindrome.\n";

	return 0;
}
/*********************************************************************
** Description: This function takes an string parameter and returns 
** true if that string is a palindrome, otherwise it returns false. 
** Parameters: string word
*********************************************************************/
bool isPalindrome(string word)
{
	for (int i = (word.length()-1); i >=0; --i)
	{
		//Return false as soon as there is a mismatch
		if ((word.at(i)) != (word.at((word.length()-i)-1)))
		{
			return false;
		}
		
	}//END OF LOOP
	return true;

}
